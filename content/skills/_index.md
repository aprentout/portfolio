+++
title = "Skills"
date = "2014-04-09"

aliases = ["skills"]
[ author ]
  name = ""

+++

```YAML
skills:
  development:
    - Javascript (Ember.js)
    - Golang
    - JSON
    - YAML

  software:
    - Git (with Git Flow)
    - Docker

  other:
    - Linux and macOs friendly
```
