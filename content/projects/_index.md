+++
title = "Projects"
date = "2022-02-02"

aliases = ["projects","skills"]
[ author ]
  name = ""

+++

```YAML
skills:
  development:
    - Javascript
    - Ember.js
    - Golang
    - JSON
    - YAML

  software:
    - Git (with Git Flow)
    - Docker

  other:
    - Linux and macOs friendly
```
