# Portfolio GoHugo

Projet de portfolio pour www.aprentout.com

‘‘‘bash
#Generate cmd
hugo
’’’

# To DO

>- [x] Nettoyer la config
>- [x] Mettre a jour hugo

>- [x] Gerer erreur 404
>- [ ] Gerer robot.txt // TODO
>- [ ] Gerer les droit d'acces (.htacces) // STARTED

>- [ ] Gerer le nom de l'onglet

> - [ ] Deployer une premiere version
> - [ ] Layout pour projet
> - [ ] Layout pour etudes
> - [ ] Layout pour experiences
> - [ ] Modifier le title (topbar) en fonction de l'emplacement sur le site


> - [ ] Gerer le multilingue
>- [ ] Ecrire les articles en francais
>- [ ] Ecrire les articles en anglais
